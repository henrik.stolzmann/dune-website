Global Functions Treffen August 2013
====================================

Organisatorisches
-----------------

-   Anreise ist bis **Montag Mittag**.

    *  13:00 Mensa
    *  14:00 Beginn (für alle die es nicht zur Mensa schaffen)

-   Übernachten kann man z.B. im Hotel am
    Schlosspark (http://www.hotel-am-schlosspark-muenster.de/) - von
    dort kann man bequem in die Stadt oder zum Institut laufen. Wir
    hatten da immer wieder Leute einquartiert und bekamen damals auch
    einen gewissen Rabatt eingeräumt. Unsere Sekretärin kann ich euch da
    noch mehr zu sagen (carolin.gietz@uni-muenster.de).

Agenda
------

1.  Übersichtsvorträge PDELab, FUFEM, FEM
2.  Diskussion Design: Das Design eines neuen Dune Moduls für
    globale Funktionen. Sollte also insbesondere beeinhalten

    * Funktionenraumbasen
    * Mapper
    * Vektor-Backends
    * Behandlung von Constraints (hängende Knoten, Dirichlet-Werte, periodische Randwerte, ...)
    * Interface für Ableitungen

und so sein, dass es alles erlaubt, was bisher in fem, pdelab oder fufem
geht.

1.  Diskussion LocalFunctions: In diesem Zuge werden ein paar Fragen
    bzgl localfunctions wieder auf den Tisch kommen, z.B. global-valued
    local functions.

    * permutation (aka twist)
    * globalwertige lokale Funtionen
    * local-to-global wrapper

Material
--------

-   Vortrag PDELab (Steffen)
-   Vortrag FUFEM (Carsten)
-   [Vortrag FEM](/pdf/meetings/2013-08-globalfunctions/dune-fem-talk.pdf) (Christoph Gersbacher, Stefan Girke)
-   Proposal globalwertige lokale Funktionen (Jö)\
    *Siehe [dune-localfunctions-manual.pdf](/pdf/meetings/2013-08-globalfunctions/dune-localfunctions-manual.pdf) Section 4*
