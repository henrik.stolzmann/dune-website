User Meeting 2012 canceled
==========================

**The DUNE User Meeting 2012 is canceled. The facts that we have three
days of PDESoft before and that it overlaps with the developers meeting
did not create enough interest.**

[Read the cancellation
announcement](https://lists.dune-project.org/pipermail/dune/2012-May/011312.html)

### Former invitation

After the International Workshop “PDE Software Frameworks - 10th
Anniversary of DUNE,”\
http://pdesoft2012.uni-muenster.de, June 18–20, 2012, Münster,
Germany,\
there will be the opportunity to have the 2nd DUNE User Meeting.\
That means the evening of 20.6. and the full day 21.6.2012.\
Please **add your name** below if you would like to participate.\
Please also feel free to contribute your ideas.

### Formerly prospective Participants

|**Name**|**Institution**|
 ------- | ------------- |
|Bernd Flemisch|University of Stuttgart|
|Felix Albrecht|University of Münster|
|Lars Lubkoll|Zuse Institute Berlin|
|Christoph Grüninger|University of Stuttgart|
|Atgeirr Rasmussen|SINTEF, Oslo|
|Andrea Sacconi|Imperial College London|
|Matthias Wohlmuth|TU München|

### Former Ideas

Please add your your idea to the following list, each as a separate wiki
page:

-   [[idea\_short\_demonstrations|Short demonstrations]]
-   [[idea\_dune\_hack\_sprint|Dune hack sprint]]
-   [[idea\_webinar|Webinar]]
