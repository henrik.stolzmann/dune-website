+++
title = "Buildsystem Docs"
layout = "buildsystem"
[menu.main]
parent = "docs"
identifier = "buildsystem"
weight = 5
+++

# DUNE Build System Documentation

Note that [The Dune Book](https://www.springer.com/gp/book/9783030597016)
contains a chapter about the build system.

Beginning from version 2.4, DUNE's CMake build system is documented with Sphinx.
Please choose the documentation you want to view from the following list.
