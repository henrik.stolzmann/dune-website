+++
date = "2020-11-27T13:35:00+01:00"
version = "2.7.1"
major_version = "2"
minor_version = "7"
patch_version = "1"
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-grid-howto", "dune-localfunctions"]
signed = 1
title = "Dune 2.7.1"
doxygen_branch = ["v2.7.1"]
doxygen_url = ["/doxygen/2.7.1"]
doxygen_name = ["Dune Core Modules"]
doxygen_version = ["2.7.1"]
support = ["linux","windows"]
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.7.1 - Release Notes

DUNE 2.7.1 is a bugfix release, but also includes some minor adjustments.
See below for a list of changes.

- Fixed installation issues where files were not installed.
- Compilation is possible in C++20 mode, but the required compiler and
  C++ version remain the same as for DUNE 2.7.0.

## Module dune-istl:
- Several fixed for solver factory.

## Module dune-localfunctions:
- Fixed Lagrange element for (3d) pyramids.
