+++
group = "extension"
title = "Extension Modules"
[menu.main]
parent = "modules"
weight = 4
+++

Some Dune modules extend the functionality of Dune greatly while not being core
modules. These are listed in this category.
