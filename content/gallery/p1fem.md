+++
title = "P1 Finite Element Examples"
content = "carousel"
image = ["/img/alberta2d.jpg", "/img/alberta3d.jpg", "/img/alucube3d.jpg", "/img/alusimplex3d.jpg", "/img/iso.jpg", "/img/ugcube2d.jpg", "/img/ugcube3d.jpg", "/img/ugsimplex2d.jpg", "/img/ugsimplex3d.jpg", "/img/yasp3d.jpg"]
+++

Results of the same finite element application running on different grids.

Details can be found in:

P. Bastian, M. Blatt, A. Dedner, C. Engwer, R. Klöfkorn, R. Kornhuber, M. Ohlberger, O. Sander. *A Generic Grid Interface for Parallel and Adaptive Scientific Computing. Part II: Implementation and Tests in DUNE.* [Computing 82(2-3):121-138, 2008](http://www.springerlink.com/content/gn177r643q2168g7/), [Preprint](http://www.matheon.de/research/show_preprint.asp?action=details&serial=404).

<!--more-->

From left to right the images show:

1. 2D simulation using Dune::Grid::Alberta
1. 3D simulation using Dune::Grid::Alberta
1. 3D simulation using Dune::Grid::ALU3dGrid with cubes
1. 3D simulation using Dune::Grid::ALU3dGrid with simplices
1. Isosurface of a 3D simulation
1. 2D simulation using Dune::Grid::UGGrid with cubes
1. 3D simulation using Dune::Grid::UGGrid with cubes
1. 3D simulation using Dune::Grid::UGGrid with simplices
1. 3D simulation using Dune::Grid::UGGrid with simplices
1. 3D simulation using Dune::Grid::YaspGrid
