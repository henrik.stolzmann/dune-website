+++
title = "Cahn-Larché"
content = "carousel"
image = ["/img/cahnlarche-1.png","/img/cahnlarche-2.png","/img/cahnlarche-3.png","/img/cahnlarche-4.png"]
+++

Solution snapshots of the *Cahn-Larché equation*
on a locally refined UGGrid.
C. Gräser, R. Kornhuber, and U. Sack. Numerical simulation of coarsening in binary solder alloys. Comp. Mater. Sci., 93:221--233, 2014
[doi](http://dx.doi.org/10.1016/j.commatsci.2014.06.010)

<!--more-->
