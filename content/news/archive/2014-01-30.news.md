+++
date = "2014-01-30"
title = "DUNE-FEM Course (February 27 to March 07, 2014)"
+++

The principal focus of the course is on the discretization of diffusion dominated boundary problems with continuous finite elements with the DUNE-FEM discretization module. Naturally, this implies that the course has also to give some insight into C++ programming techniques, installation of DUNE, usage of the DUNE-core modules during the first days.

Lessons will be daily, 09:00-15:00 in the first week and 09:00-12:30 in the second week. The focus of the \`\`free time'' in the second week will be on a self-dependent implementation of a model problem, including verification and visualization of the simulation results.

The course forms an elective part of the regular M.Sc. curriculum at the Math Department at the University of Stuttgart, which explains the "uncompact" format of the block-course. However, participants from other universities are welcome as long as there are free capacities. External participants will be charged a fee of 50 EUR for course material and infrastructure (coffee breaks).

*Contact and course venue:*
 <Claus-Justus.Heine@IANS.Uni-Stuttgart.DE>
 Numerical Mathematics for High Performance Computing (NMH)
 Institute for Applied Analysis and Numerical Simulation (IANS)
 Faculty of Mathematics and Physics, Stuttgart University
 Pfaffenwaldring 57
 70569 Stuttgart
