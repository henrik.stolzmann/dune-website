+++
date = "2008-04-02"
title = "New tutorial on writing grid implementations"
+++

We have started to work on a new tutorial module called [dune-grid-dev-howto](http://www.dune-project.org/downloadsvn.html). This module will collect information on how to write new grid managers for the DUNE grid interface and how to adapt existing grid codes to DUNE. In particular it contains a dummy grid manager called `IdentityGrid`. It wraps a given second DUNE grid and includes all the boilerplate code that you need for your own grids. This should get you started on your own grid implementations. In the long run the module will also contain a document describing how to implement new DUNE Grids.
