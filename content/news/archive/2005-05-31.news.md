+++
date = "2005-05-31"
title = "Public bug tracking system"
+++

We recently activated an open-access [Bug tracking system](http://www.flyspray.org/). Everybody is invited to check for known bugs and file new reports or feature requests.

*Update: we switched to the gitlab internal [bug tracker](/dev/issues/)*
