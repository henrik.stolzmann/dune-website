+++
date = "2010-09-08"
title = "Exherbo Packages Available"
+++

Packages for the [Exherbo](http://www.exherbo.org) Linux distribution are now available. They are kindly provided by Elias Pipping.
