+++
date = "2014-01-13"
title = "DUNE/PDELab Course (March 24-28, 2014)"
+++

This one week course will provide an introduction to the most important DUNE modules and especially to DUNE-PDELab. At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results. Topics covered are the solution of stationary and time-dependent problems, as well as local adaptivity, the use of parallel computers and the solution of non-linear PDE's and systems of PDE's.

Registration deadline: Friday March 7, 2014 Dates: March 24, 2014 - March 28, 2014

*Course venue:* Interdisciplinary Center for Scientific Computing
 University of Heidelberg, Im Neuenheimer Feld 350/368
 69120 Heidelberg, Germany

*Fee:* The fee for this course is 200 EUR including course material, coffee and lunch breaks as well as course dinner on Wednesday.

*For registration and further information* see <http://conan.iwr.uni-heidelberg.de/dune-workshop/>.
