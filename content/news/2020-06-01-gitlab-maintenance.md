+++
date = "2020-06-01"
title = "Gitlab Maintenance on 8th/9th June"
+++

We will do some gitlab maintenance on the 8th and 9th of June. This means you
won't be able to reach

https://gitlab.dune-project.org/

during this time. Maintenance work will start on monday the 8th of June at 9am
CEST (=UTC+2).
