+++
date = "2018-11-05"
title = "hp-DG for two-phase flow in porous media"
# content = "carousel"
# image = ["/img/hpdg-twophase-flow.png"]
package = "dune-fem"
tags = [ "publications", "dune-fem", "python" ]
url = ["paper","https://dx.doi.org/10.1016/j.apm.2018.10.013"]
+++

The [Python framework for hp-adaptive discontinuous Galerkin methods for two-phase flow in porous media][hpdg-paper]
was recently published. The paper presents a hp-adaptive Discontinuous Galerkin
approach for two-phase flow in porous media. The implementation is based in the
newly developed Python binding for DUNE and DUNE-FEM. A [Docker
image][docker-img] can be found quick testing of the available features.

[hpdg-paper]: https://dx.doi.org/10.1016/j.apm.2018.10.013
[docker-img]: https://gitlab.dune-project.org/dune-fem/twophaseflow
