+++
# The name of the module.
module = "dune-vtk"
group = ["extension"]
requires = ["dune-grid"]
suggests = ["dune-functions","dune-spgrid","dune-polygongrid","dune-alugrid","dune-foamgrid","dune-uggrid"]
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de)"
git = "https://gitlab.dune-project.org/extensions/dune-vtk"
short = "File reader and writer for the VTK Format"
+++
# VTK Reader and Writer
This module provides structured and unstructured file writers for the VTK XML File Formats
that can be opened in the popular [ParaView](https://www.paraview.org/) visualization application.
Additionally a file reader is provided to import VTK files into Dune grid and data objects.

Compared to the classical Dune-Grid VTKWriter it supports some new features:

* User-selected floating point precision
* Writing StructuredGrid, RectilinearGrid, and ImageData
* Compressed (appended) binary format
* Writing parametrized elements with quadratic interpolation
* Higher-order Lagrange parametrized Elements and Data

The VtkReader supports UnstructuredGrid files in *ASCII*, *BINARY*, or *COMPRESSED* format and
can be used in combination with a GridFactory.
