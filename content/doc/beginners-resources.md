+++
title = "Beginners Resources"
[menu.main]
identifier = "beginnersresources"
parent = "docs"
weight = 0
+++

Before you begin, we'd like to point out the Dune book:

<a href="https://www.springer.com/gp/book/9783030597016">
<img src="/img/dune-book-cover.png" width="200" />
</a>

It contains a wealth of information about the Dune system: Its design ideas,
main features and interfaces, and many tips and tricks.  There is
a chapter on how to install Dune and get started, and many complete
example programs showcasing features of various Dune modules.

# How to get started with Dune

You want to install and use Dune on your machine and you are a complete
novice? Then this is the right place to start!

We will give a short introduction to Dune and guide you through the
installation process.

Dune is a software framework  for the numerical solution of partial
differential equations (PDEs) written in C++. This means that it provides a
set of classes that help you to write your own PDE solver. It is not
an application with a fancy graphical user interface that you can just
run, type in your PDE and look at the result. Instead you write a C++
program that includes various pieces from the framework and glues them
together to solve a particular PDE with a particular method. It is,
however, quite flexible in letting you implement various different
solution procedures. Dune does also provide Python bindings for a large
part of its core features which can help with the rapid prototyping of new
code or for pre- and postprocessing tasks.

There are a number of different ways how to install and use Dune on
your computer (*click on the links to follow the instructions*):

1. Follow the detailed instructions in Oliver Sander's document on
[how to get started with Dune.](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf)
This document (it is actually one chapter from [The Book](https://www.springer.com/gp/book/9783030597016))
describes installation from binary packages and
installation from the source repository. It also describes how to
solve a simple PDE with the Dune core modules.

2. [Installation of Dune from binary packages on Debian and Ubuntu systems](/doc/beginners-resources-binary).
   This is the most convenient way if you have such a system
   and do not want to modify the Dune sources.

3. [Installation of Dune from the Python Package Index (PyPi)](/doc/installation-pip).
   This is another very convenient way to install Dune which does not
   require root access and works on a broad range of systems including
   MacOS for which binary packages might not be available.

4. [Installation from source via a shell script](/doc/beginners-resources-script).
   This is the proper way if binary packages
   are not available for your machine and/or you want to modify the Dune
   sources.

Instructions for installing [dune-PDELab](/modules/dune-pdelab) from
the source are available [here](/doc/beginners-resources-pdelab).
Detailed instructions on how to install [dune-fem](/modules/dune-fem) and its Python bindings
using `pip`, a `docker image`, or from source
are available [here](/doc/beginners-resources-dunefem).

## Computer Requirements

It is important to note that installation of Dune requires a computer system
with a relatively recent operating system, either
Linux or Apple MacOS. Windows is not officially
supported. Installation on Windows has been
managed by some but it is definitely not something to try if you are a beginner.

If you are running Windows the best approach to getting started is to
set up a virtual machine using
[VirtualBox](https://www.virtualbox.org) and install Linux in it,
e.g. [Ubuntu Linux](http://www.ubuntu.com).
