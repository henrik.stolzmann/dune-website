+++
title = "Installation"
toc = 1
[menu.main]
parent = "docs"
weight = 1
+++

## Dependencies
In order to build DUNE you need at least the following software:

* C++ compiler supporting c++-17 language standard, e.g. [clang](https://clang.llvm.org/) >= 5, [g++](https://gcc.gnu.org/) >= 7
* [CMake](https://cmake.org) >= 3.13
* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/)

Detailed information on supported compiler and CMake versions can be found in the [release notes](/releases) for releases and in the list [recent changes](/dev/recent-changes) for the development branch *master*. See also the [FAQ section](#faq) for information about how to resolve the build dependencies.

The following software is recommend but optional:

* MPI (e.g. [OpenMPI](https://www.open-mpi.org/) or [MPICH](https://www.mpich.org/))

This will provide you with the core DUNE features.

Some DUNE modules might support further software. At the end of the configuration process of each dune module, a list of found, optional and required dependencies is listed.

## Getting the sources

First you need to download the DUNE core modules to your computer in one common directory. You can either download [tarballs of the last releases of the core modules](/releases) or download [directly from our git repositories](/groups/core). Note: If you download the modules directly from git make sure to checkout a release branch if you don't want to work with the current development version.


## Building DUNE modules

To compile the modules DUNE has to check several components of your system and whether prerequisites within the modules are met. For the ease of users we have designed a custom utility on top of CMake, `dunecontrol`. This can be found in the directory `dune-common/bin/`. Run

```
./dune-common/bin/dunecontrol [OPTIONS] cmake [CMAKE_FLAGS]
./dune-common/bin/dunecontrol [OPTIONS] make [MAKE_FLAGS]
```

to configure and build all modules you have downloaded in the order of resolved dependencies. Instead of executing the commands `cmake` and `make` separately, it can be combined into one, called `all`:

```
./dune-common/bin/dunecontrol [OPTIONS] all
```

Then `CMAKE_FLAGS` and `MAKE_FLAGS` have to be passed using either environmental variables, or a options file, see below.

From now on, we assume that `dunecontrol` is in your `PATH`, e.g., by running

```
echo 'export PATH=/path/to/dune-common/bin:${PATH}' >> ${HOME}/.bashrc
source ${HOME}/.bashrc
```


### Building other DUNE modules
Besides the core modules you can find lots of other dune modules:

- [Grid Modules](/groups/grid)
- [Discretization Modules](/groups/disc)
- [Extension Modules](/groups/extension)
- [Tutorial Modules](/groups/tutorial)

If you want to use one of those modules make sure to download the module and all dependencies in the same common directory as the DUNE core modules. Building your modules is done in the same way as building the core modules by calling `dunecontrol`.

### Building a specific DUNE module (and its dependent modules)

You can instruct `dunecontrol` to build only a certain dune module, using the `--only=<module_name>` switch. Running `dunecontrol` script
```
dunecontrol --only=<module_name> all
```

where `<module_name>` is the name of that particular module given in the `dune.module` file,
will build only the module `<module_name>`.

If you want to build a module and the modules it depends on, you must run:
```
dunecontrol --module=<module_name> all
```

### Changing build mode and optimization flags

The flags passed to the compiler are controlled by CMake. Thus, in order to change these flags, you have to add a `CMAKE_FLAG` responsible for compiler (and/or linker) options.

One can either specify a global build mode:
```
dunecontrol cmake -DCMAKE_BUILD_TYPE=[Debug|MinSizeRel|RelWithDebInfo|Release]
```

(see [CMake documentation](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html) for a more detailed description of the CMake option)

or you can specify compiler flags explicitly, e.g.,
```
dunecontrol cmake -DCMAKE_CXX_FLAGS="-O3 -DNDEBUG -march=native"
```

This is equivalent to specifying both, the release more and the additional `-march=native` flag:
```
dunecontrol cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-march=native"
```

Both flags can be put into an options file, see below, or can be set by specifying the `CMAKE_FLAGS` environmental variable:
```
CMAKE_FLAGS='-DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-march=native"' dunecontrol cmake
```

Note, if you do not specify anything, the CMake defaults are used which essentially means: no code optimization.

### Speeding up the build process

Building several DUNE modules can be quiet time consuming. Using a concurrent build can speed this up. Either you can instruct `make` to use multiple threads when building,

```
dunecontrol make -jN
```

or you can specify another make-system, like [Ninja build](https://ninja-build.org), by setting a different CMake generator:

```
dunecontrol cmake -GNinja
dunecontrol make
```

If parts of the code must be rebuild multiple times, a utility like [ccache](https://ccache.dev) might be useful and can be specified as a `COMPILER_LAUNCHER` in CMake:
```
dunecontrol cmake -DCMAKE_CXX_COMPILER_LAUNCHER=/path/to/ccache
```

(see [CMake documentation](https://cmake.org/cmake/help/latest/envvar/CMAKE_LANG_COMPILER_LAUNCHER.html) for a more detailed description of the CMake option)

### Storing flags in an options file

As it is often not convenient to specify the desired options after the `dunecontrol` call, one can pass the options via file specified by the `--opts` option:
```
dunecontrol --opts=<file> COMMAND
```

Possible options for the options-file (or the environment) are

* `CMAKE_FLAGS`: arguments passed to the `cmake` command
* `MAKE_FLAGS`: arguments passed to the `make` command
* `BUILDDIR`: build directory for an out-of-source build. default: `build-cmake`
* `CMAKE`: executable to use for `cmake`
* `DUNE_CONTROL_PATH`: A PATH variable to locate dune modules in its subdirectories, directories are separated by colon `:`

An example of an options file is
```bash
# set build mode and compiler flags,
# install to a custom directory,
# disable the MPI library,
# add a search path for external libraries
# and use Ninja-build instead of make as the build-tool
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release \
             -DCMAKE_CXX_FLAGS=\"-march=native\" \
             -DCMAKE_INSTALL_PREFIX=/path/where/to/install/dune \
             -DCMAKE_DISABLE_FIND_PACKAGE_MPI=1 \
             -DCMAKE_PREFIX_PATH=\"/path/to/lib1;/path/to/lib2\" \
             -GNinja"
```


## Details about the `dunecontrol` utility

The general structure of a `dunecontrol` call is
```
dunecontrol [OPTIONS] COMMAND
```

### Commands
The following atomic `COMMAND`s can be executed:

* `cmake`: run the CMake system for each module and passes along the directories of the found dune modules
* `make`: run make (or any other build tool that was configured) for each module
* `all`: combination of `cmake` and `make`
* `exec`: execute a command in each module directory
* `bexec`: execute a command in each module's build directory
* `update`: pull the latest version from the Git repository
* `git`: run a specific git command in all module directories

### Options
The following general `OPTIONS` are available:

* `--help`: Show a detailed list of all options with description.
* `--module=<mod>`, `--only=<mod>`, `--current`, `--current-dep`: Control which modules to build, see above.
* `--builddir=<dir>`: Make out-of-source builds in a subdir `<dir>`. This directory is created inside each module. If `<dir>` is an absolute path, the build directory is set to `<dir>/module-name` for each module.
* `--opts=<file>`: load default options from `<file>`


## Creating your own DUNE project module
You can create your own dune project module by using the `duneproject` script available in `dune-common/bin` directory. Running the script will create a directory with supporting files (`CMakeLists.txt` etc.) and a sample .cc file. After creating the module you can build this as explained above under "Building a specific DUNE module".

The [DUNE Build System Documentation](/sphinx/core/) will also give you an excellent introduction to the build system and how to create new modules/projects your own.


## Installing DUNE in HPC Clusters
Cluster architectures often have a linux based system that is quiet different from a desktop environment, with its own package management system that needs special handling of the installation such that everything is found properly. However, often it is enough to just load the (build) dependencies and build DUNE using `dunecontrol` the classical way as described above. There are also some *non-official* ways of building DUNE in HPC environments, e.g.

- There is a DUNE package for the [Spack](https://spack.io) package management system, see https://gitlab.dune-project.org/spack/dune-spack for more information
- Installing DUNE in a system using [EasyBuild](https://easybuilders.github.io/easybuild) as software management system requires the description of the DUNE modules and its dependencies in EasyConfig files. See https://gitlab.mn.tu-dresden.de/spraetor/dune-easybuild for some examples.
- Some old instructions for the BlueGene-P system can be found in the [Installation Notes for Blue Gene P](docs/installation-bluegenep).

Warning: those descriptions and tools are often provided just for a single or a few DUNE releases and are sometimes outdated.


## FAQ
### My System does not provide the required minimal compiler version. Where can I find updates?
This heavily depends on your system. For Debian/Ubuntu based Linux distributions, sources are:

- PPA for recent gcc/g++ compilers for various ubuntu distributions: https://launchpad.net/~ubuntu-toolchain-r/+archive/ubuntu/test
- APT repository for clang (debian and ubuntu packages): https://apt.llvm.org

Follow the instruction on the referenced pages.

### How can I update my system CMake version?
Similar to the compilers, it depends on your distribution:

- APT repository from Kitware: https://apt.kitware.com
- Snap package: https://snapcraft.io/cmake (for RHEL, see https://snapcraft.io/install/cmake/rhel)
- Using PIP: `python -m pip install cmake`
- docker container including CMake: e.g. https://registry.dune-project.org/docker/ci/ubuntu:20.04 or https://registry.dune-project.org/docker/ci/debian:10
- Binary from Kitware website: https://cmake.org/download + add `CMAKE="YOUR_PATH/bin/cmake"` to your options file.
- Compile CMake from source: https://cmake.org/download, using your old CMake.

### My dune packages are located in different directories. How can I find them using dunecontrol?
The `dunecontrol` script searches in the current directory and all its subdirectories for files name `dune.module`.
Those files contain the information about the corresponding dune module and its dependencies. Additionally,
`dunecontrol` searches in all directories (and subdirectories of those) listed in the variable
`DUNE_CONTROL_PATH`. Similarly to the `PATH` variable, directories are separated by colon `:`.

You can either prefix the call of `dunecontrol` to individually specify the search paths, i.e.,
```
DUNE_CONTROL_PATH=path/to/module1:/path/to/module2 dunecontrol [...] COMMAND
```

or you can put this path information into an options file, next to the other options like `CMAKE_FLAGS` or
`MAKE_FLAGS`.
