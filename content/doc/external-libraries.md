+++
title= "External Libraries"
[menu.main]
parent = "docs"
weight = 2
+++

## Installing External Libraries
There are various external libraries that you can use together with DUNE. These include grid managers, file format libraries, fast linear algebra packages and more. On this page we list some of them and explain how they are obtained and installed.

### Grid Managers
{{<table>}}
| Name                    | description           |
|---                               |---          |
| [ALBERTA](/doc/install-alberta") | A grid manager for 2d and 3d simplicial grid with bisection refinement. Needed to use the AlbertaGrid class. |
{{</table>}}

### File I/O
{{<table>}}
| Name                    | description           |
|---                               |---          |
| [Gmsh](/doc/install-gmsh)  | An open-source finite element mesh generator with basic CAD capabilities. Meshes can be imported by Dune. |
{{</table>}}

### Others
{{<table>}}
| Name                    | description           |
|---                               |---          |
| [psurface](/doc/install-psurface)   | A library for handling boundary parametrizations. |
{{</table>}}

### dune-istl
{{<table>}}
| Name                    | description           |
|---                               |---          |
| [SuperLU](/doc/install-superlu)     | General purpose library for the direct solution of large, sparse, nonsymmetric systems. |
| SuiteSparse                      | Collection of sparse matrix algorithms and solvers. |
{{</table>}}
