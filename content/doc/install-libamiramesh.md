+++
title = "Installing libamiramesh"
+++
### Installing libamiramesh for DUNE
`libamiramesh` is a library which provides reading and writing facilities for the AmiraMesh format, which is the native format of the [Amira](https://www.fei.com/software/amira-for-life-sciences/) visualization system. DUNE grids can be read from and written as AmiraMesh files if this library is installed.

`libamiramesh` is no longer available as a downloadable, precompiled binary.

After you somehow got and unpacked the library, it is sufficient to add
```
--with-amiramesh=libamiramesh
```

as an option to configure of dune-grid, where libamiramesh is the directory the library tarball unpacks into.
