+++
title = "Using Dune and its Python bindings from PyPi, Docker/Vagrant, or from source"
[menu.main]
parent = "beginnersresources"
+++

There are a number of ways to use DUNE and it's Python bindings.
The easiest way is to install the required Dune packages from the Python
package index `PyPi` into a virtual environment using `pip`.
This requires at least a working Python and C++ installation on the host
system. An alternative is to use a docker or Vagrant image. Finally for
more advanced users the installation from source is recommended.

The pip installation is a straightforward as typing
```
pip install dune-fem
```
If your system does not have the required compilers installed,
a [script](https://gitlab.dune-project.org/dune-fem/dune-fem-dev/raw/master/rundocker.sh)
is available that can be used for the docker installation:
simply typing
```
python myscript.py
```
gets the docker container up and running which still allows
editing and post-processing of your data on your host as usual.
Obviously this method of installation requires having Docker installed.

See the [installation instructions](/sphinx/content/sphinx/dune-fem/installation.html)
which covere all the above methods.
